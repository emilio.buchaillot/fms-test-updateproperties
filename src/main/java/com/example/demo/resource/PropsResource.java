package com.example.demo.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

@RestController
@RequestMapping(path="/props")
public class PropsResource {
    private Logger LOG = LoggerFactory.getLogger(PropsResource.class);
    protected Properties props = new Properties();
    @Value("${file.name}")
    private String stringValue;

    @GetMapping("/prueba")
    public String pruebaGet() {
        LOG.info("Hello");
        return stringValue;
    }

    @PostMapping("/properties")
    public void updateNetworkElements(@RequestBody Map<String, String> map) {
            saveProp(stringValue,map);
            LOG.info("String elements " + map);
    }

    public void saveProp(String propFileName, Map<String, String> map) {
        String fileName = propFileName;
        try {
            props.putAll(map);
            props.store(new FileOutputStream(fileName), "Property created at " + new Date());
        } catch (Exception e) {
            LOG.error("Error saving properties '" + fileName + "'", e);
        }
    }

}
